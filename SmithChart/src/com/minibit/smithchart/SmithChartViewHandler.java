package com.minibit.smithchart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class SmithChartViewHandler extends SmithChartBaseThread
{
	private Bitmap backgroundMask;
	private Paint foregroundElementPaint;
	private Complex gamma = new Complex();

	private float screenWidth, screenHeight;
	private float screenSideDimension;
	private float padding;

	public SmithChartViewHandler(Context context, SurfaceHolder holder)
	{
		super(context, holder);

		foregroundElementPaint = new Paint();
		foregroundElementPaint.setColor(Color.GREEN);
		foregroundElementPaint.setStyle(Style.STROKE);
		foregroundElementPaint.setStrokeWidth(3.0f);
		foregroundElementPaint.setAntiAlias(true);
		foregroundElementPaint.setDither(true);
		foregroundElementPaint.setStrokeWidth(3.5f);

		padding = SmithChartUtils.convertPixelsToDp(10, context);
		setScreenDimensions(holder.getSurfaceFrame().width(), holder.getSurfaceFrame().height());

		initBackgroundMask(screenWidth, screenHeight);
	}

	@Override
	public void update()
	{

	}

	@Override
	public void draw(Canvas canvas)
	{
		// One side might be longer than the other; center every element in the Smith Chart.
		float dx = screenWidth > screenSideDimension ? (screenWidth - screenSideDimension) / 2.0f : 0;
		float dy = screenHeight > screenSideDimension ? (screenHeight - screenSideDimension) / 2.0f : 0;
		canvas.translate(dx + padding, dy + padding);

		// Clear the background
		canvas.drawARGB(255, 0, 0, 0);
		foregroundElementPaint.setStyle(Style.STROKE);
		foregroundElementPaint.setColor(Color.CYAN);
		drawRightAttachedCircle(canvas);
		foregroundElementPaint.setColor(Color.RED);
		drawCentralCircle(canvas);

		foregroundElementPaint.setColor(Color.GREEN);
		drawTopArc(canvas);

		foregroundElementPaint.setColor(Color.BLUE);
		drawTouchCircle(canvas);
		foregroundElementPaint.setColor(Color.WHITE);
		drawStaticBackground(canvas);

		canvas.drawCircle(screenSideDimension / 2, screenSideDimension / 2, screenSideDimension / 2, foregroundElementPaint);

		canvas.translate(-dx - padding, -dy - padding);
		// Draw the overlay mask so we only see stuff inside the unit circle
		canvas.drawBitmap(backgroundMask, 0, 0, foregroundElementPaint);
	}

	private void drawRightAttachedCircle(Canvas canvas)
	{
		float r = SmithChartUtils.zForGamma(gamma).getReal();
		float radius = 1 / (1 + r);
		float centerX = 1.0f - radius;
		float centerY = 0.0f;
		PointF circleCenter = SmithChartUtils.worldForLocalCoordinates(new PointF(centerX, centerY), screenSideDimension);
		canvas.drawCircle(circleCenter.x, circleCenter.y, screenSideDimension - circleCenter.x, foregroundElementPaint);
	}

	private void drawCentralCircle(Canvas canvas)
	{
		float localRadius = (float) Math.sqrt(gamma.getReal() * gamma.getReal() + gamma.getImaginary() * gamma.getImaginary());
		float radius = localRadius * screenSideDimension / 2;
		float centerX = screenSideDimension / 2;
		float centerY = screenSideDimension / 2;
		canvas.drawCircle(centerX, centerY, radius, foregroundElementPaint);
	}

	private void drawTouchCircle(Canvas canvas)
	{
		PointF touchLoc = SmithChartUtils.worldForLocalCoordinates(gamma, screenSideDimension);
		canvas.drawCircle(touchLoc.x, touchLoc.y, 7.0f, foregroundElementPaint);
	}

	private void drawStaticBackground(Canvas canvas)
	{
		float radius = screenSideDimension / 2; // Radius of 1 in local coords
		canvas.drawCircle(screenSideDimension, screenSideDimension, radius, foregroundElementPaint);
		canvas.drawCircle(screenSideDimension, 0, radius, foregroundElementPaint);
		canvas.drawCircle(screenSideDimension / 2 + radius / 2, screenSideDimension / 2, radius / 2, foregroundElementPaint);
		canvas.drawCircle(screenSideDimension / 2 + radius / 2 + radius / 4, screenSideDimension / 2, radius / 4, foregroundElementPaint);
		canvas.drawLine(0, screenSideDimension / 2, screenSideDimension, screenSideDimension / 2, foregroundElementPaint);
	}

	private void drawTopArc(Canvas canvas)
	{
		float z = SmithChartUtils.zForGamma(gamma).getImaginary();
		float localRadius = 1 / z;
		float x1 = 0;
		float y1 = 0;
		float r1 = 1;

		float x2 = 1;
		float y2 = localRadius;
		float r2 = localRadius;

		float d = (float) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

		float xi = 0;
		float yi = 0;

		if (localRadius > 0)
		{
			xi = (float) ((x2 + x1) / 2.0 + ((x2 - x1) * (Math.pow(r1, 2) - Math.pow(r2, 2))) / (2 * Math.pow(d, 2)) - ((y2 - y1) / (2 * Math.pow(d,
					2))) * Math.sqrt((Math.pow(r1 + r2, 2) - Math.pow(d, 2)) * (Math.pow(d, 2) - Math.pow(r2 - r1, 2))));

			yi = (float) ((y2 + y1) / 2.0 + ((y2 - y1) * (Math.pow(r1, 2) - Math.pow(r2, 2))) / (2 * Math.pow(d, 2)) + ((x2 - x1) / (2 * Math.pow(d,
					2))) * Math.sqrt((Math.pow(r1 + r2, 2) - Math.pow(d, 2)) * (Math.pow(d, 2) - Math.pow(r2 - r1, 2))));
		}
		else
		{
			xi = (float) ((x2 + x1) / 2.0 + ((x2 - x1) * (Math.pow(r1, 2) - Math.pow(r2, 2))) / (2 * Math.pow(d, 2)) + ((y2 - y1) / (2 * Math.pow(d,
					2))) * Math.sqrt((Math.pow(r1 + r2, 2) - Math.pow(d, 2)) * (Math.pow(d, 2) - Math.pow(r2 - r1, 2))));

			yi = (float) ((y2 + y1) / 2.0 + ((y2 - y1) * (Math.pow(r1, 2) - Math.pow(r2, 2))) / (2 * Math.pow(d, 2)) - ((x2 - x1) / (2 * Math.pow(d,
					2))) * Math.sqrt((Math.pow(r1 + r2, 2) - Math.pow(d, 2)) * (Math.pow(d, 2) - Math.pow(r2 - r1, 2))));
		}

		PointF startPoint = SmithChartUtils.worldForLocalCoordinates(new PointF(xi, yi), screenSideDimension);
		float radius = Math.abs(localRadius * screenSideDimension / 2);
		if (yi > 0.0f)
		{
			canvas.drawCircle(screenSideDimension, screenSideDimension / 2 - radius, radius, foregroundElementPaint);
		}
		else
		{
			canvas.drawCircle(screenSideDimension, screenSideDimension / 2 + radius, radius, foregroundElementPaint);
		}
	}

	@Override
	public void onTouch(MotionEvent event)
	{
		super.onTouch(event);
		Complex oldCoordinates = new Complex(gamma);
		SmithChartUtils.localForWorldCoordinates(getTouchCoords(), screenSideDimension, gamma);
		float newRadius = (float) Math.sqrt(gamma.getReal() * gamma.getReal() + gamma.getImaginary() * gamma.getImaginary());
		if (newRadius >= 1.0f)
		{
			gamma.setReal(oldCoordinates.getReal());
			gamma.setImaginary(oldCoordinates.getImaginary());
		}
	}

	public void updateGammaValues(float gammaReal, float gammaImaginary)
	{
		Complex oldCoordinates = new Complex(gamma);
		gamma.setReal(gammaReal);
		gamma.setImaginary(gammaImaginary);
		float newRadius = (float) Math.sqrt(gamma.getReal() * gamma.getReal() + gamma.getImaginary() * gamma.getImaginary());
		if (newRadius >= 1.0f)
		{
			gamma.setReal(oldCoordinates.getReal());
			gamma.setImaginary(oldCoordinates.getImaginary());
		}
	}

	public Complex getGamma()
	{
		return gamma;
	}

	public float getScreenSideDimension()
	{
		return screenSideDimension;
	}

	public void setScreenDimensions(float width, float height)
	{
		screenWidth = width;
		screenHeight = height;

		// Factor in the padding ONLY after we have initialized the background mask, which should have no padding at all
		screenWidth -= padding * 2.0f;
		screenHeight -= padding * 2.0f;

		screenSideDimension = Math.min(screenWidth, screenHeight);
		initBackgroundMask(width, height);

	}

	public void initBackgroundMask(float screenWidth, float screenHeight)
	{
		if (backgroundMask != null)
		{
			backgroundMask.recycle();
		}

		backgroundMask = Bitmap.createBitmap((int) screenWidth, (int) screenHeight, Config.ARGB_8888);
		Canvas maskCanvas = new Canvas(backgroundMask);
		maskCanvas.drawARGB(255, 0, 0, 0);
		Paint maskPaint = new Paint(foregroundElementPaint);
		maskPaint.setColor(Color.BLUE);
		maskPaint.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));
		maskPaint.setStyle(Style.FILL);

		// One side might be longer than the other; center every element in the Smith Chart.
		float dx = screenWidth > screenSideDimension ? (screenWidth - screenSideDimension) / 2.0f : 0;
		float dy = screenHeight > screenSideDimension ? (screenHeight - screenSideDimension) / 2.0f : 0;
		maskCanvas.drawCircle(screenSideDimension / 2 + dx, screenSideDimension / 2 + dy, screenSideDimension / 2, maskPaint);
	}
}
