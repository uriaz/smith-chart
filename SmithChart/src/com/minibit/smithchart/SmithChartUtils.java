package com.minibit.smithchart;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SmithChartUtils
{
	public static Complex zForGamma(Complex gamma)
	{
		Complex numerator = new Complex(-gamma.getReal() - 1.0f, -gamma.getImaginary());
		Complex denom = new Complex(gamma.getReal() - 1.0f, gamma.getImaginary());
		return new Complex(numerator.div(denom));
	}

	public static Complex gammaForZ(Complex z)
	{
		Complex numerator = new Complex(z.getReal() + 1, z.getImaginary());
		Complex denom = new Complex(z.getReal() - 1, z.getImaginary());
		return new Complex(numerator.div(denom));
	}

	// Width and height will always be equal.
	public static Complex localForWorldCoordinates(PointF worldCoords, float screenSide)
	{
		float localX = worldCoords.x / screenSide * 2 - 1;
		float localY = -(worldCoords.y / screenSide * 2 - 1);
		return new Complex(localX, localY);
	}

	public static void localForWorldCoordinates(PointF worldCoords, float screenSide, Complex result)
	{
		float localX = (worldCoords.x / screenSide) * 2 - 1;
		float localY = -((worldCoords.y / screenSide) * 2 - 1);
		result.setReal(localX);
		result.setImaginary(localY);
	}

	public static PointF worldForLocalCoordinates(Complex localCoordinates, float screenSide)
	{
		float worldX = (float) (localCoordinates.getReal() + 1.0f) / 2.0f * (screenSide);
		float worldY = -(float) (localCoordinates.getImaginary() - 1.0f) / 2.0f * (screenSide);
		return new PointF(worldX, worldY);
	}

	public static void worldForLocalCoordinates(Complex localCoordinates, float screenSide, PointF result)
	{
		float worldX = (float) (localCoordinates.getReal() + 1.0f) / 2.0f * (screenSide);
		float worldY = -(float) (localCoordinates.getImaginary() - 1.0f) / 2.0f * (screenSide);
		result.set(worldX, worldY);
	}

	public static void worldForLocalCoordinates(PointF localCoordinates, float screenSide, PointF result)
	{
		float worldX = (float) (localCoordinates.x + 1.0f) / 2.0f * (screenSide);
		float worldY = -(float) (localCoordinates.y - 1.0f) / 2.0f * (screenSide);
		result.set(worldX, worldY);
	}

	public static PointF worldForLocalCoordinates(PointF localCoordinates, float screenSide)
	{
		float worldX = (float) (localCoordinates.x + 1.0f) / 2.0f * (screenSide);
		float worldY = -(float) (localCoordinates.y - 1.0f) / 2.0f * (screenSide);
		return new PointF(worldX, worldY);
	}

	/**
	 * This method converts dp unit to equivalent pixels, depending on device density.
	 * 
	 * @param dp
	 *            A value in dp (density independent pixels) unit. Which we need to convert into pixels
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent px equivalent to dp depending on device density
	 */
	public static float convertDpToPixel(float dp, Context context)
	{
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float px = dp * (metrics.densityDpi / 160f);
		return px;
	}

	/**
	 * This method converts device specific pixels to density independent pixels.
	 * 
	 * @param px
	 *            A value in px (pixels) unit. Which we need to convert into db
	 * @param context
	 *            Context to get resources and device specific display metrics
	 * @return A float value to represent dp equivalent to px value
	 */
	public static float convertPixelsToDp(float px, Context context)
	{
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

	private static Typeface defaultFont;
	private static Typeface boldDefaultFont;
	public static String DEFAULT_FONT = "helvetica_light.otf";
	public static String BOLD_DEFAULT_FONT = "helvetica_bold.otf";

	public static Typeface getDefaultFont(Context context)
	{
		if (defaultFont == null)
		{
			defaultFont = Typeface.createFromAsset(context.getAssets(), DEFAULT_FONT);
		}

		return defaultFont;
	}

	public static Typeface getBoldDefaultFont(Context context)
	{
		if (boldDefaultFont == null)
		{
			boldDefaultFont = Typeface.createFromAsset(context.getAssets(), BOLD_DEFAULT_FONT);
		}

		return boldDefaultFont;
	}

	public static void overrideFonts(final Context context, final View v)
	{
		Typeface font = getDefaultFont(context);

		try
		{
			if (v instanceof ViewGroup)
			{
				// Bootstrap button uses font-awesome; don't screw with that shit
				ViewGroup vg = (ViewGroup) v;
				for (int i = 0; i < vg.getChildCount(); i++)
				{
					View child = vg.getChildAt(i);
					overrideFonts(context, child);
				}
			}
			else if (v instanceof TextView)
			{
				((TextView) v).setTypeface(font);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private final static String RATING_KEY = "ratingKey";
	private final static String USAGE_KEY = "usageKey";

	public static void setShouldAskForRating(boolean shouldAsk, Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		prefs.edit().putBoolean(RATING_KEY, shouldAsk).commit();
	}

	public static boolean shouldAskForRating(Context context)
	{
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(RATING_KEY, true);
	}

	public static void incrementUseCounter(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int prevUse = prefs.getInt(USAGE_KEY, 0);
		prefs.edit().putInt(USAGE_KEY, prevUse + 1).commit();
	}

	public static int getUseCounter(Context context)
	{
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getInt(USAGE_KEY, 0);
	}
}
