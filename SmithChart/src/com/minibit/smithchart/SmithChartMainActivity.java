package com.minibit.smithchart;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class SmithChartMainActivity extends FragmentActivity implements OnSeekBarChangeListener, SmithChartDataTransfer
{
	private SeekBar gammaRealSeekBar, gammaImaginarySeekBar;
	private SmithChartView smithChartView;
	private TextView gammaValuesTextView;

	private Complex currentGammaValue = new Complex();

	public final static String GAMMA_VALUES_FILE_KEY = "KEY";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_smith_chart_main);

		smithChartView = (SmithChartView) findViewById(R.id.smithChartView);
		smithChartView.setOnTouchListener(smithChartListener);
		smithChartView.setListener(this);

		gammaRealSeekBar = (SeekBar) findViewById(R.id.gammaRealSlider);
		gammaRealSeekBar.setOnSeekBarChangeListener(this);
		gammaRealSeekBar.setMax(100);

		gammaImaginarySeekBar = (SeekBar) findViewById(R.id.gammaImaginarySlider);
		gammaImaginarySeekBar.setOnSeekBarChangeListener(this);
		gammaImaginarySeekBar.setMax(100);

		gammaValuesTextView = (TextView) findViewById(R.id.gammaValuesTextView);

		SmithChartUtils.overrideFonts(this, gammaValuesTextView.getRootView());
		if (SmithChartUtils.getUseCounter(this) > 5 && SmithChartUtils.shouldAskForRating(this))
		{
			displayRatingDialog();
		}

		SmithChartUtils.incrementUseCounter(this);
	}

	private OnTouchListener smithChartListener = new OnTouchListener()
	{
		@Override
		public boolean onTouch(View touchedView, MotionEvent event)
		{
			if (event.getX() >= 0.0f && event.getY() >= 0.0f && event.getX() <= touchedView.getWidth() && event.getY() <= touchedView.getHeight())
			{
				smithChartView.getViewHandler().onTouch(event);
				currentGammaValue = smithChartView.getViewHandler().getGamma();
				int gammaRealSlider = (int) ((currentGammaValue.getReal() + 1) * gammaRealSeekBar.getMax() / 2);
				int gammaImaginarySlider = (int) ((currentGammaValue.getImaginary() + 1) * gammaRealSeekBar.getMax() / 2);
				gammaRealSeekBar.setProgress(gammaRealSlider);
				gammaImaginarySeekBar.setProgress(gammaImaginarySlider);
				updateGammaTextField();
			}
			return true;
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_smith_chart_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.menu_settings)
		{
			AboutDialog dialog = new AboutDialog();
			dialog.show(getSupportFragmentManager(), "About dialog");
		}

		return true;
	}

	private float gammaReal = 0.0f;
	private float gammaImaginary = 0.0f;
	private int previousGammaRealValue, previousGammaImaginaryValue;

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
	{
		if (fromUser)
		{
			float sliderProgress = progress;
			sliderProgress = sliderProgress / seekBar.getMax() * 2 - 1;

			if (seekBar == gammaRealSeekBar)
			{
				previousGammaRealValue = (int) ((gammaReal + 1) / 2 * seekBar.getMax());
				gammaReal = sliderProgress;
			}
			else if (seekBar == gammaImaginarySeekBar)
			{
				previousGammaImaginaryValue = (int) ((gammaImaginary + 1) / 2 * seekBar.getMax());
				gammaImaginary = sliderProgress;
			}

			boolean isInBounds = Math.sqrt(gammaReal * gammaReal + gammaImaginary * gammaImaginary) <= 1.0f ? true : false;

			if (smithChartView != null && smithChartView.getViewHandler() != null)
			{
				// if (isInBounds)
				// {
				if (seekBar == gammaRealSeekBar)
				{
					smithChartView.getViewHandler().updateGammaValues(gammaReal, gammaImaginary);
				}
				else if (seekBar == gammaImaginarySeekBar)
				{
					smithChartView.getViewHandler().updateGammaValues(gammaReal, gammaImaginary);
				}
				// }
				/*
				 * else { if (seekBar == gammaRealSeekBar) { //gammaRealSeekBar.setProgress((int) previousGammaRealValue); gammaReal =
				 * previousGammaRealValue; } else if (seekBar == gammaImaginarySeekBar) { //gammaImaginarySeekBar.setProgress((int)
				 * previousGammaImaginaryValue); gammaImaginary = previousGammaImaginaryValue; } }
				 */
			}
			updateGammaTextField();
		}
	}

	public void updateGammaTextField()
	{
		if (smithChartView != null && smithChartView.getViewHandler() != null)
		{
			Complex gamma = smithChartView.getViewHandler().getGamma();

			float real = gamma.getReal();
			String realString = real + "";
			if (realString.length() >= 8)
			{
				realString = realString.substring(0, 8);
			}

			float imaginary = gamma.getImaginary();
			String imaginaryString = imaginary + "";
			if (imaginaryString.length() >= 8)
			{
				imaginaryString = imaginaryString.substring(0, 8);
			}

			char gammaSymbol = (char) 915;
			String gammaValuesText = gammaSymbol + "r: " + realString + ",    " + gammaSymbol + "i: " + imaginaryString;
			gammaValuesTextView.setText(gammaValuesText);

		}
	}

	public void saveGammaValues()
	{
		SharedPreferences preferences = getSharedPreferences(GAMMA_VALUES_FILE_KEY, 0);
		Editor editor = preferences.edit();
		if (smithChartView != null && smithChartView.getHandler() != null)
		{
			Complex gamma = smithChartView.getViewHandler().getGamma();
			editor.putString(GAMMA_VALUES_FILE_KEY, gamma.getReal() + "," + gamma.getImaginary());
			editor.commit();
		}
	}

	public void loadGammaValues()
	{
		SharedPreferences preferences = getSharedPreferences(GAMMA_VALUES_FILE_KEY, 0);
		String gammaValue = preferences.getString(GAMMA_VALUES_FILE_KEY, "0.0,0.0");
		String real = gammaValue.split(",")[0];
		String imag = gammaValue.split(",")[1];
		float gammaReal = Float.parseFloat(real);
		float gammaImag = Float.parseFloat(imag);
		smithChartView.getViewHandler().updateGammaValues(gammaReal, gammaImag);

		currentGammaValue = smithChartView.getViewHandler().getGamma();
		int gammaRealSlider = (int) ((currentGammaValue.getReal() + 1) * gammaRealSeekBar.getMax() / 2);
		int gammaImaginarySlider = (int) ((currentGammaValue.getImaginary() + 1) * gammaRealSeekBar.getMax() / 2);
		gammaRealSeekBar.setProgress(gammaRealSlider);
		gammaImaginarySeekBar.setProgress(gammaImaginarySlider);
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		saveGammaValues();
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{

	}

	@Override
	public void screenVisible(boolean isVisible)
	{
		if (isVisible)
		{
			loadGammaValues();
			updateGammaTextField();
		}
	}

	private void displayRatingDialog()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_info)).setTitle("Feedback").setMessage("Are you enjoying this app?")
				.setPositiveButton("Yes", new OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						AlertDialog.Builder ratingDialog = new AlertDialog.Builder(SmithChartMainActivity.this);
						ratingDialog.setTitle("Feedback").setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_info))
								.setMessage("We'd love to hear from you. Would you like to leave a rating?")
								.setPositiveButton(getResources().getString(android.R.string.ok), new OnClickListener()
								{
									@Override
									public void onClick(DialogInterface dialog, int which)
									{
										startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.minibit.smithchart")));
										SmithChartUtils.setShouldAskForRating(false, SmithChartMainActivity.this);
									}
								}).setNegativeButton("No thanks", new OnClickListener()
								{
									@Override
									public void onClick(DialogInterface dialog, int which)
									{
										SmithChartUtils.setShouldAskForRating(false, SmithChartMainActivity.this);
									}
								});

						ratingDialog.create().show();
					}
				}).setNegativeButton("No", new OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						SmithChartUtils.setShouldAskForRating(false, SmithChartMainActivity.this);
					}
				}).setNeutralButton("Don't ask again", new OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						SmithChartUtils.setShouldAskForRating(false, SmithChartMainActivity.this);
					}
				});

		AlertDialog feelingMeasureDialog = builder.create();
		feelingMeasureDialog.show();
	}

}
