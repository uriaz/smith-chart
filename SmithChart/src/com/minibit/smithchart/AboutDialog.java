package com.minibit.smithchart;

import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.SpannableString;

public class AboutDialog extends DialogFragment
{
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		SpannableString message = new SpannableString("Developed by Usama Riaz.\n\n� Minibit Technologies " + year);
		builder.setMessage(message).setTitle("About");
		builder.setPositiveButton("View More Apps", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				// market://search?q=pub:<publisher_name>
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://search?q=pub:Minibit Technologies"));
				startActivity(intent);
			}
		});
		
		builder.setNegativeButton(getActivity().getResources().getString(android.R.string.cancel), null);
		Dialog dialog = builder.create();
		return dialog;
	}
}
