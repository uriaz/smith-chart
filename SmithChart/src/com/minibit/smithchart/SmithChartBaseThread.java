package com.minibit.smithchart;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class SmithChartBaseThread
{
	public Handler threadHandler = new Handler();
	public Runnable refreshRunnable = new Runnable()
	{

		@Override
		public void run()
		{
			runThread();
		}
	};

	private Context context;
	private SurfaceHolder surfaceHolder;
	private int FPS = 60;

	public SmithChartBaseThread(Context context, SurfaceHolder holder)
	{
		this.context = context;
		this.surfaceHolder = holder;
	}

	public void update()
	{

	}

	public void draw(Canvas canvas)
	{

	}

	public void runThread()
	{
		SurfaceHolder holder = surfaceHolder;

		Canvas canvas = null;
		try
		{
			canvas = holder.lockCanvas();
			if (canvas != null)
			{
				update();
				draw(canvas);
			}
		}
		finally
		{
			if (canvas != null)
				holder.unlockCanvasAndPost(canvas);
		}

		threadHandler.removeCallbacks(refreshRunnable);
		long refreshRate = 1000;
		if (FPS == 0)
		{
			refreshRate /= 1;
		}
		else
		{
			refreshRate /= FPS;
		}

		threadHandler.postDelayed(refreshRunnable, refreshRate);
	}

	private PointF touchCoords = new PointF();

	public void onTouch(MotionEvent event)
	{
		touchCoords.set(event.getX(), event.getY());
	}

	public PointF getTouchCoords()
	{
		return touchCoords;
	}

	public void setTouchCoords(PointF touchCoords)
	{
		this.touchCoords = touchCoords;
	}

}
