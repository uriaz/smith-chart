package com.minibit.smithchart;

public class Complex
{
	public Complex()
	{
		this.re = 0;
		this.im = 0;
	}

	public Complex(float re, float im)
	{
		this.re = re;
		this.im = im;
	}

	public Complex(Complex input)
	{
		this.re = input.getReal();
		this.im = input.getImaginary();
	}

	public float getReal()
	{
		return this.re;
	}

	public float getImaginary()
	{
		return this.im;
	}

	public void setReal(float re)
	{
		this.re = re;
	}

	public void setImaginary(float im)
	{
		this.im = im;
	}

	public Complex add(Complex op)
	{
		Complex result = new Complex();
		result.setReal(this.re + op.getReal());
		result.setImaginary(this.im + op.getImaginary());
		return result;
	}

	public Complex sub(Complex op)
	{
		Complex result = new Complex();
		result.setReal(this.re - op.getReal());
		result.setImaginary(this.im - op.getImaginary());
		return result;
	}

	public Complex mul(Complex op)
	{
		Complex result = new Complex();
		result.setReal(this.re * op.getReal() - this.im * op.getImaginary());
		result.setImaginary(this.re * op.getImaginary() + this.im * op.getReal());
		return result;
	}

	public Complex div(Complex op)
	{
		Complex result = new Complex(this);
		result = result.mul(op.getConjugate());
		float opNormSq = op.getReal() * op.getReal() + op.getImaginary() * op.getImaginary();
		result.setReal(result.getReal() / opNormSq);
		result.setImaginary(result.getImaginary() / opNormSq);
		return result;
	}

	public Complex fromPolar(float magnitude, float angle)
	{
		Complex result = new Complex();
		result.setReal((float) (magnitude * Math.cos(angle)));
		result.setImaginary((float) (magnitude * Math.sin(angle)));
		return result;
	}

	public float getNorm()
	{
		return (float) Math.sqrt(this.re * this.re + this.im * this.im);
	}

	public float getAngle()
	{
		return (float) Math.atan2(this.im, this.re);
	}

	public Complex getConjugate()
	{
		return new Complex(this.re, this.im * (-1));
	}

	public String toString()
	{
		if (this.re == 0)
		{
			if (this.im == 0)
			{
				return "0";
			}
			else
			{
				return (this.im + "i");
			}
		}
		else
		{
			if (this.im == 0)
			{
				return String.valueOf(this.re);
			}
			else if (this.im < 0)
			{
				return (this.re + " " + this.im + "i");
			}
			else
			{
				return (this.re + " +" + this.im + "i");
			}
		}
	}

	private float re;
	private float im;

}
