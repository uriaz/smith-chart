package com.minibit.smithchart;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

public class SmithChartView extends SurfaceView implements Callback, OnTouchListener
{
	private Context context;
	private SmithChartViewHandler viewHandler;
	private SmithChartDataTransfer listener;

	public SmithChartView(Context context, AttributeSet attributeSet)
	{
		super(context, attributeSet);
		getHolder().addCallback(this);
		this.context = context;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if (event.getX() >= 0.0f && event.getY() >= 0.0f && event.getX() <= this.getWidth() && event.getY() <= this.getHeight())
		{
			viewHandler.onTouch(event);
		}

		return true;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
		viewHandler.setScreenDimensions(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		viewHandler = new SmithChartViewHandler(context, holder);
		viewHandler.runThread();
		listener.screenVisible(true);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		viewHandler.threadHandler.removeCallbacks(viewHandler.refreshRunnable);
	}

	public SmithChartViewHandler getViewHandler()
	{
		return viewHandler;
	}

	public void setViewHandler(SmithChartViewHandler viewHandler)
	{
		this.viewHandler = viewHandler;
	}

	public SmithChartDataTransfer getListener()
	{
		return listener;
	}

	public void setListener(SmithChartDataTransfer listener)
	{
		this.listener = listener;
	}

}
